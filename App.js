import * as React from 'react';
import { 
  View,Text,ImageBackground,StyleSheet, Image
} from 'react-native';
import {NavigationContainer } from '@react-navigation/native';
import { createStackNavigator} from '@react-navigation/stack';
import Login from './screens/login';
import Lista from './screens/lista';
import Detalles from './screens/detalles';
const Stack = createStackNavigator();


export default () =>{
  return(
  <NavigationContainer>
    <Stack.Navigator initialRouteName="Inicio" /*screenOptions={{ }}*/ >
      <Stack.Screen options={{ headerShown: false,title: 'Ingreso', 
      headerStyle: {
          backgroundColor: '#0B2161',
        },headerTintColor: '#ffd60a',
        headerTitleStyle: {
          fontWeight: 'bold',
        },headerTitleAlign:'center'}} 
      name="Inicio" component={Login}/>

      <Stack.Screen options={{title:`Jet Set`
        ,headerStyle: {
          backgroundColor: '#0B2161',
        },headerTintColor: '#ffd60a',
        headerTitleStyle: {
          fontWeight: 'bold',
        },headerTitleAlign:'center'
      }} 
      name='Lista' component={Lista} />
      
      <Stack.Screen options={
        backgroundColor='#000',
        ({ route }) => ({ 
          title: `${route.params.equipos.nombre}`
          ,headerTitleAlign: 'center',
          headerStyle:{
            backgroundColor:'rgba(11, 33, 97, 0.98)', 
          },headerTintColor:`${route.params.equipos.color}`,
        })
      }
      name='Detalles' component={Detalles} />
    </Stack.Navigator>
  </NavigationContainer> 
)};