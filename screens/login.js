import React from 'react';
import {View,Text,StyleSheet,TextInput,ImageBackground, Button} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icons from 'react-native-vector-icons/Entypo';
import { useNavigation } from '@react-navigation/native';

const Usuario='David';
const Clave='123456';
function Ingreso({user, cla}){
    if(Usuario==user && Clave==cla){
        alert('Ingresando...');
    }else{
        alert('Credenciales invalidas...');
    }
}

export default login => {
    const navigation = useNavigation();
    return(
        <View style={estilos.container}>
            <ImageBackground 
            style={estilos.backgroundImg} 
            source={require('../Img/BG.jpg')}>
                <View style={estilos.difuminado}> 
                <View style={estilos.form}>
                    <Text style={estilos.titulo}>
                        <Icons name='login' size={40}></Icons>Login
                    </Text>
                    <View style={{flexDirection:'row', 
                        flex:0, alignSelf: 'center',marginTop:20,}}>
                        <Icon name="user" size={40} color="#000" style={estilos.icono} />
                        <TextInput placeholderTextColor='#fff' placeholder='Usuario' 
                        style={estilos.input} />
                    </View>
                    <View style={{flexDirection:'row', 
                        flex:0, alignSelf: 'center',marginBottom:30}}>
                        <Icon name="lock" size={40} color="#000" style={estilos.icono} />
                        <TextInput placeholderTextColor='#fff' placeholder='********' 
                        style={estilos.input} secureTextEntry={true} />
                    </View>
                    <Button title='Ingresar' color='rgba(255,214,10,0.9)' style={{
                        marginTop:10,width:'100%',textAlign: 'center',
                        fontWeight: '700'}}
                        onPress={() => navigation.push('Lista')}/>
                </View>
                </View>    
            </ImageBackground>
        </View>
    );
}
const estilos = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
    },
    backgroundImg:{
        flex:0, resizeMode:'contain',
        width: '100%', height: '100%',
    },
    difuminado:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        textAlignVertical: 'center',
        height:'100%',width:'100%',
        backgroundColor:'rgba(11, 33, 97, 0.4)',
    },
    form:{
        flex: 0,
        alignItems:'center', justifyContent:'center',
        padding:20,
        backgroundColor: 'rgba(11, 33, 97, 0.8)', 
        height: '40%', width: '80%',
        borderTopEndRadius: 40,
        borderBottomLeftRadius: 40,
    },
    titulo:{
        letterSpacing: 2,
        fontFamily: 'sans-serif',
        fontSize:40,
        textTransform:'uppercase',
        color:'#ffd60a',paddingBottom:10,
    },
    icono:{
        flex:1, textAlign: 'center', 
        justifyContent:'center',
        alignContent:'center', 
        backgroundColor:'#ffd60a',
    },
    input:{
        flex:9,
        height: 50, width: '100%', 
        borderColor: '#fff', 
        borderWidth: 1, borderTopRightRadius:0, 
        color: '#fff',
        paddingLeft: '2%',
        fontSize:20,
    },
});