import React from "react";
import {
  View, Text,StyleSheet,Image, ImageBackground, Button,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';

const backGImg = 'https://external-preview.redd.it/SX9YD-XX6SO2EyhQEjZaj-7W48bWYibMmcDfXk1FJUg.jpg?s=a5443c24f1d431bd81701a372dca6d4b677a0b6e';

export default ({ route }) => {
  const equipos = route.params.equipos;
  return <DetallesView 
    nombre={equipos.nombre} 
    caption={equipos.titulo} 
    img={equipos.img} 
    desc={equipos.descrip}
    color={equipos.color} />
};

export function DetallesView({nombre,caption,img,desc,color}){
  const navigation = useNavigation();
  return(
    
    <View style={estilos.details}>
        <ImageBackground 
          style={estilos.ImgBackground} 
          source={{ uri:backGImg, }} >
            <Image style={estilos.img} source={{uri:img,}} />
            <View style={estilos.desContainer}>
              <Text style={{
                color:`${color}`,
                fontSize:40,
                fontWeight:"bold",
                textAlign:"center",
              }}> {nombre} </Text>
              <Text style={estilos.caption}> {caption} </Text>
              <Text style={estilos.description}> {desc} </Text>
              <View style={{flex:0,justifyContent:'center',flexDirection:'row',}}>
                <Button color='rgba(79, 214, 10, 1)' title="Login" 
                  onPress={()=>{navigation.navigate('Inicio')}}
                  />
                <Button color='rgba(190, 3, 0, 1)' title="A la lista" 
                  onPress={()=>{navigation.navigate('Lista')}}
                  />
              </View>
            </View>
        </ImageBackground>
    </View>
  );
};

const estilos = StyleSheet.create({
  container:{ flex: 1,  },
  ImgBackground:{
    resizeMode: "contain",
    width: '100%',
    height: '100%',   
  },
  img:{
    margin:10,
    alignSelf: 'center',
    width:180,
    height:220,
    resizeMode: 'contain',
  },
  desContainer:{ 
    alignSelf:'center',
    padding:'4%',margin:'8%',
    backgroundColor:'rgba(11, 33, 97, 0.8)',
    borderRadius:40, borderWidth:2,
  },
  caption:{
    alignSelf:'center',
    fontSize: 25,
    margin:10,
    color: '#fff',
  },
  description:{
    fontSize:20,
    color:'#fff',
  },
});
