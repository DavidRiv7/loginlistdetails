import React, { Component } from "react";
import {
  FlatList,View,Text,
  StyleSheet,Image,
  ImageBackground,Button,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
const backgroundImg = 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwallpapercave.com%2Fwp%2Fwp3885941.jpg&f=1&nofb=1';

function Boton({ screenName, datos }) {
  const navigation = useNavigation();
  return (
    <Button
      style={styles.boton}
      title={`${screenName}`}
      color="rgba(255, 214, 10, 0.9)"
      onPress={() => navigation.push(screenName, {equipos:datos})}
    />
  );
}

function Listar({img,titulo,nombre,datos}){ 
  return(
    <View style={styles.item}>
        <View style={styles.left}>
            <Image source={{uri: img}} style={styles.imagen} />
        </View>
        <View style={styles.right}>
            <Text style={styles.nombre}>
            Equipo: {nombre}</Text>
            <Text style={styles.desc}> {titulo} </Text>
            <Boton screenName="Detalles" datos={datos}/>
        </View>
        
    </View>    
  ) 
}

export default class Lista extends Component {

    listaVacia = [];
  futbolTeams= [
    {key:'0',nombre:'Boca Juniors', titulo:'El más grande.',
     color:'#ffd60a',
     img:'https://dc562.4shared.com/img/a8Lj7Dxnea/s24/171c340b128/Boca_Juniors?async&rand=0.9500212193307667',
     descrip:'Boca Juniors es una entidad deportiva argentina con sede en el barrio de La Boca, Buenos Aires. Fue fundado en dicho barrio el 3 de abril de 1905 por seis vecinos adolescentes hijos de italianos.  El fútbol es su disciplina más destacada, aunque también compite a nivel profesional, nacional e internacionalmente, en baloncesto, voleibol, futsal y fútbol femenino, mientras que deportes como el boxeo, judo, karate, taekwondo, lucha, gimnasia rítmica y artística se practican a nivel amateur.'},
    {key:'1',nombre:'River Plate', titulo:'Quemaron su estadio.', 
     color:'#FF0000',
     descrip:'El club fue fundado el 25 de mayo de 1901 en el barrio de La Boca, tras la fusión de los clubes Santa Rosa y La Rosales. Su nombre proviene de la antigua denominación que se le daba en inglés británico al Río de la Plata.',
     img:'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fen%2Fthumb%2F4%2F43%2FClub_Atl%25C3%25A9tico_River_Plate_logo.svg%2F1200px-Club_Atl%25C3%25A9tico_River_Plate_logo.svg.png&f=1&nofb=1'},
    {key:'2',nombre:'Real Madrid', titulo:'Rey de europa.',
    color:'#ffd60a',
    descrip:'Fundado el 6 de marzo de 1902 como Madrid Football Club, el club ha usado tradicionalmente un equipo blanco de casa desde el inicio. La palabra real es español para "real" y fue otorgada al club por el rey Alfonso XIII en 1920 junto con la corona real en el emblema. El equipo ha jugado sus partidos en casa en el estadio Santiago Bernabéu con capacidad para 81.044 espectadores en el centro de Madrid desde 1947. A diferencia de la mayoría de las entidades deportivas europeas, los miembros del Real Madrid ( socios ) han sido dueños y operadores del club a lo largo de su historia.', 
     img:'http://1.bp.blogspot.com/-BitjWByz2F0/UU5oo-7q0hI/AAAAAAAAApM/wmxTIxuz1ss/s1600/Real_madrid_logo.png'},
    {key:'3',nombre:'A. Madrid', titulo:'<<Equipo del pueblo>>.',
    color:'#ffd60a', 
    descrip:'Atlético o Atleti , es un club de fútbol profesional español con sede en Madrid , que juega en la liga . El club juega sus partidos en casa en el Estadio Metropolitano , que tiene una capacidad de 68,456.', 
     img:'https://dc562.4shared.com/img/G8N2EUcwea/s24/171c3408a18/Atleti?async&rand=0.3001050188564305'},
    {key:'4',nombre:'Liverpool', titulo:'Mejor equipo actual.', 
    color:'#FF0000', 
    descrip:'El Liverpool Football Club es un club de fútbol profesional con sede en Liverpool, Inglaterra, que disputa la Premier League, máxima competición futbolística en ese país. Fue oficialmente registrado como club de fútbol por John Houlding el 6 de junio de 1892, aunque originalmente fue nombrado «Everton F.C. and Athletic Grounds Ltd» ante la Asociación Inglesa de Fútbol.', 
     img:'https://4.bp.blogspot.com/-QiLMp_ntJ00/UCQPfMn-wHI/AAAAAAAAATk/uMWxZ96M1eE/s1600/455pxLiverpool_FCn_logo.svg.png'},
    {key:'5',nombre:'M. City', titulo:'Qatar Inc.', 
    color:'#ffd60a', 
    descrip:`El Manchester City Football Club es un club de fútbol de Mánchester, Inglaterra que juega en la Premier League. Fue fundado en 1880 bajo el nombre de St. Mark's, luego pasó a llamarse Ardwick Association Football Club en 1887 y finalmente, el 16 de abril de 1894, se convirtió en el Manchester City.`, 
     img:'https://i.pinimg.com/originals/0e/ff/7e/0eff7e487c0da743888af142b43fb2aa.png'},
    {key:'6',nombre:'PSG', titulo:'Qatar Inc.', 
    color:'#FF0000', 
    descrip:'París Saint-Germain Football Club, conocido popularmente por sus siglas PSG, es un club de fútbol profesional con sede en París, Francia. El club fue fundado el 12 de agosto de 1970 gracias a la fusión del Paris Football Club y el Stade Saint-Germanois.', 
     img:'https://www.futbolprimera.es/files/psg_badge.png'},
    {key:'7',nombre:'Inter de Milan', titulo:'Reviviendo...', 
    color:'#ffd60a', 
    descrip:'El Inter de Milán, mejor conocido como Internazionale o simplemente Inter, es un club de fútbol de Italia con sede de la ciudad de Milán, capital de la región de Lombardía. Fue fundado el 9 de marzo de 1908, bajo el nombre de Foot-Ball Club Internazionale por cuarenta y cuatro disidentes del Milan Cricket & Football Club, actual A. C. Milan, que en ese tiempo no aceptaba extranjeros en sus filas.', 
     img:'https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/FC_Internazionale_Milano_2014.svg/1200px-FC_Internazionale_Milano_2014.svg.png'},
    {key:'8',nombre:'Juventus', titulo:'Hegemonía local.',
    color:'#ffd60a', 
    descrip:'La Juventus de Turín, conocida simplemente por su nombre, Juventus, o popularmente como la Juve, es un club de fútbol italiano con sede en la ciudad de Turín, capital de la región del Piamonte. Fue fundado el 1 de noviembre de 1897 con el nombre de «Sport Club Juventus» por un grupo de estudiantes locales.', 
     img:'https://www.logofootball.net/wp-content/uploads/Juventus-FC-HD-Logo-750x750.png'},
    {key:'9',nombre:'Bayer Munich', titulo:'Desfasados...',
    color:'#FF0000', 
    descrip:'None', 
     img:'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/FC_Bayern_M%C3%BCnchen_logo_%282017%29.svg/1200px-FC_Bayern_M%C3%BCnchen_logo_%282017%29.svg.png'},
    {key:'10',nombre:'Borussia Dormunt', titulo:'Semillero, vendedor.',
    color:'#ffd60a', 
    descrip:'None', 
     img:'http://3.bp.blogspot.com/-itmM8lc8v-Y/UJnNrSab9zI/AAAAAAAALK4/fBP1Ea0x3K8/s1600/Borussia+Dortmund.png'},
  ] 
  separador(){
    return(
      <View style={{ 
        height:2,width:'100%',backgroundColor:'rgba(255, 214, 10, 0.4)',marginVertical:10}}>
      </View>
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={{ uri:backgroundImg }} 
        style={styles.ImgBackground} blurRadius={0.4}>
          
          <View style={styles.listContainar}>
            <FlatList style={styles.lista} 
            /*data={this.futbolTeams}*/
            data={this.futbolTeams}
            keyExtractor={item => item.key}
            renderItem={ ({item}) => {
                return(
                  <Listar 
                    id={item.key} img={item.img} titulo={item.titulo} 
                    des={item.descrip} nombre={item.nombre} datos={item}
                  />
                );
            }}
            ItemSeparatorComponent={this.separador}
            ListEmptyComponent={
                <View>
                    <Text style={{fontSize:20, color:'#fff', textAlign: 'center'}}>No se encuentran datos para mostrar...</Text>
                </View>
              }
            />
          </View>
        
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{ flex: 1,  },
  MainText:{
    fontSize: 24,
    color: 'orange',
    fontWeight: 'bold',
    textTransform: 'capitalize',
    textAlign: "center",
  },
  ImgBackground:{
    flex: 1,
    resizeMode: "stretch",
    width: '100%',
    height: '100%',   
  },
  listContainar:{
    backgroundColor: 'rgba(11, 33, 97, 0.9)',
    flex: 0,
    margin: 26,
    height: '90%',
    borderRadius: 20,
    borderWidth: 2,
    borderColor: 'rgba(255, 214, 10, 0.4)',
  },
  imagen:{
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    resizeMode: 'contain',
    height:'100%', width: '60%',
  },
  item:{
    marginTop:1, 
    flex: 1,
    flexDirection:"row"
  },
  left:{
      flex: 1,
      alignItems: "flex-end",
      flexDirection:"column",    
  },
  right:{ 
      flex:2,
      alignItems: "center",
      flexDirection: "column",
  },
  nombre:{
      color:'#fff',
      fontSize: 18,
  },
  desc:{
      color:'#ddd',
      fontSize: 14,
  },
  boton:{
    flex: 1,
    alignItems: 'center',
  },
  lista: {
      flex:0,
      alignContent: 'center',
      height: '100%',
      width: '100%',
      marginBottom: 20,
      marginTop: 20,
  }
});
